from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from gamma.settings import ProdConfig
from gamma import commands

db = SQLAlchemy()
migrate = Migrate()
jwt = JWTManager()


def create_app(config=ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config)

    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)
    CORS(app, resources={r"/api/*": {"origins": "*"}})

    def shell_context():
        return {
            'db': db
        }

    app.shell_context_processor(shell_context)

    app.cli.add_command(commands.test)
    app.cli.add_command(commands.clean)
    app.cli.add_command(commands.urls)
    app.cli.add_command(commands.coverage)
    app.cli.add_command(commands.add_team)
    app.cli.add_command(commands.add_player)

    from gamma.api import api_bp
    app.register_blueprint(api_bp, url_prefix='/api/v1')

    return app
