import datetime
import math
import re
import uuid
import json
from sqlalchemy.orm import backref, validates
from sqlalchemy.sql.functions import func
from sqlalchemy import ForeignKey, not_
from sqlalchemy.types import TypeDecorator, CHAR
from sqlalchemy.dialects.postgresql import UUID
from flask import url_for, current_app
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import create_access_token, create_refresh_token
from itsdangerous import URLSafeTimedSerializer

import ciso8601

from gamma.app import db
from gamma.settings import Config as app_config

# Alias common SQLAlchemy names
Column = db.Column
relationship = db.relationship

##############################################################
# Mixins and helper classes
##############################################################


class CRUDMixin(object):
    """Mixin that adds convenience methods for CRUD 
    (create, read, update, delete) operations."""

    @classmethod
    def create(cls, **kwargs):
        """Create a new record and save it the database."""
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs):
        """Update specific fields of a record."""
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        """Save the record."""
        db.session.add(self)
        if commit:  # pragma: no cover
            db.session.commit()
        return self

    def delete(self, commit=True):
        """Remove the record from the database."""
        db.session.delete(self)
        return commit and db.session.commit()


class Model(CRUDMixin, db.Model):
    """Base model class that includes CRUD and serialization methods

    Usage::
        >>> class User(Model):
        >>>     id = db.Column(db.Integer(), primary_key=True)
        >>>     email = db.Column(db.String(), index=True)
        >>>     name = db.Column(db.String())
        >>>     password = db.Column(db.String())
        >>>     posts = db.relationship('Post', backref='user', lazy='dynamic')
        >>>     ...
        >>>     default_fields = ['email', 'name']
        >>>     hidden_fields = ['password']
        >>>     readonly_fields = ['email', 'password']
        >>>
        >>> class Post(Model):
        >>>     id = db.Column(db.Integer(), primary_key=True)
        >>>     user_id = db.Column(db.String(), db.ForeignKey('user.id'), nullable=False)
        >>>     title = db.Column(db.String())
        >>>     ...
        >>>     default_fields = ['title']
        >>>     readonly_fields = ['user_id']
        >>>
        >>> model = User(email='john@localhost')
        >>> db.session.add(model)
        >>> db.session.commit()
        >>>
        >>> # update name and create a new post
        >>> validated_input = {'name': 'John', 'posts': [{'title':'My First Post'}]}
        >>> model.set_columns(**validated_input)
        >>> db.session.commit()
        >>>
        >>> print(model.to_dict(show=['password', 'posts']))
        >>> {u'email': u'john@localhost', u'posts': [{u'id': 1, u'title': u'My First Post'}], u'name': u'John', u'id': 1}
        https://gist.github.com/alanhamlett/6604662
    """

    __abstract__ = True

    # Stores changes made to this model's attributes. Can be retrieved
    # with model.changes
    _changes = {}

    def _set_columns(self, **kwargs):
        force = kwargs.get('_force')

        readonly = []
        if hasattr(self, 'readonly_fields'):
            readonly = self.readonly_fields
        if hasattr(self, 'hidden_fields'):
            readonly += self.hidden_fields

        readonly += [
            'id',
            'created',
            'updated',
            'modified',
            'created_at',
            'updated_at',
            'modified_at',
        ]

        changes = {}

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()

        for key in columns:
            allowed = True if force or key not in readonly else False
            exists = True if key in kwargs else False
            if allowed and exists:
                val = getattr(self, key)
                if val != kwargs[key]:
                    changes[key] = {'old': val, 'new': kwargs[key]}
                    setattr(self, key, kwargs[key])

        for rel in relationships:
            allowed = True if force or rel not in readonly else False
            exists = True if rel in kwargs else False
            if allowed and exists:
                is_list = self.__mapper__.relationships[rel].uselist
                if is_list:
                    valid_ids = []
                    query = getattr(self, rel)
                    cls = self.__mapper__.relationships[rel].argument()
                    for item in kwargs[rel]:
                        if 'id' in item and query.filter_by(id=item['id']).limit(1).count() == 1:
                            obj = cls.query.filter_by(id=item['id']).first()
                            col_changes = obj.set_columns(**item)
                            if col_changes:
                                col_changes['id'] = str(item['id'])
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(item['id']))
                        else:
                            col = cls()
                            col_changes = col.set_columns(**item)
                            query.append(col)
                            db.session.flush()
                            if col_changes:
                                col_changes['id'] = str(col.id)
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(col.id))

                    # delete related rows that were not in kwargs[rel]
                    for item in query.filter(not_(cls.id.in_(valid_ids))).all():
                        col_changes = {
                            'id': str(item.id),
                            'deleted': True,
                        }
                        if rel in changes:
                            changes[rel].append(col_changes)
                        else:
                            changes.update({rel: [col_changes]})
                        db.session.delete(item)

                else:
                    val = getattr(self, rel)
                    if self.__mapper__.relationships[rel].query_class is not None:
                        if val is not None:
                            col_changes = val.set_columns(**kwargs[rel])
                            if col_changes:
                                changes.update({rel: col_changes})
                    else:
                        if val != kwargs[rel]:
                            setattr(self, rel, kwargs[rel])
                            changes[rel] = {'old': val, 'new': kwargs[rel]}

        return changes

    def set_columns(self, **kwargs):
        self._changes = self._set_columns(**kwargs)
        if 'modified' in self.__table__.columns:
            self.modified = datetime.datetime.utcnow()
        if 'updated' in self.__table__.columns:
            self.updated = datetime.datetime.utcnow()
        if 'modified_at' in self.__table__.columns:
            self.modified_at = datetime.datetime.utcnow()
        if 'updated_at' in self.__table__.columns:
            self.updated_at = datetime.datetime.utcnow()
        return self._changes

    @property
    def changes(self):
        return self._changes

    def reset_changes(self):
        self._changes = {}

    def to_dict(self, show=None, hide=None, path=None, show_all=None):
        """ Return a dictionary representation of this model.
        """

        if not show:
            show = []
        if not hide:
            hide = []
        hidden = []
        if hasattr(self, 'hidden_fields'):
            hidden = self.hidden_fields
        default = []
        if hasattr(self, 'default_fields'):
            default = self.default_fields

        ret_data = {}

        if not path:
            path = self.__tablename__.lower()

            def prepend_path(item):
                item = item.lower()
                if item.split('.', 1)[0] == path:
                    return item
                if len(item) == 0:
                    return item
                if item[0] != '.':
                    item = '.%s' % item
                item = '%s%s' % (path, item)
                return item

            show[:] = [prepend_path(x) for x in show]
            hide[:] = [prepend_path(x) for x in hide]

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)

        for key in columns:
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or key is 'id' or check in show or key in default:
                ret_data[key] = getattr(self, key)

        for key in relationships:
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or check in show or key in default:
                hide.append(check)
                is_list = self.__mapper__.relationships[key].uselist
                if is_list:
                    ret_data[key] = []
                    for item in getattr(self, key):
                        ret_data[key].append(item.to_dict(
                            show=show,
                            hide=hide,
                            path=('%s.%s' % (path, key.lower())),
                            show_all=show_all,
                        ))
                else:
                    if self.__mapper__.relationships[key].query_class is not None:
                        ret_data[key] = getattr(self, key).to_dict(
                            show=show,
                            hide=hide,
                            path=('%s.%s' % (path, key.lower())),
                            show_all=show_all,
                        )
                    else:
                        ret_data[key] = getattr(self, key)

        for key in list(set(properties) - set(columns) - set(relationships)):
            if key.startswith('_'):
                continue
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or check in show or key in default:
                val = getattr(self, key)
                try:
                    ret_data[key] = json.loads(json.dumps(val))
                except:
                    pass

        return ret_data


class SurrogatePK(object):
    """A mixin that adds a surrogate integer 'primary key' 
    column named ``id`` to any declarative-mapped class."""

    __table_args__ = {'extend_existing': True}

    id = Column(db.Integer, primary_key=True)

    @classmethod
    def get_by_id(cls, record_id):
        """Get record by ID."""
        if any(
                (isinstance(record_id, basestring) and record_id.isdigit(),
                 isinstance(record_id, (int, float))),
        ):
            return cls.query.get(int(record_id))
        return None


def reference_col(tablename, nullable=False, pk_name='id', **kwargs):
    """Column that adds primary key foreign key reference.
    Usage: ::
        category_id = reference_col('category')
        category = relationship('Category', backref='categories')
    """
    return Column(
        db.ForeignKey('{0}.{1}'.format(tablename, pk_name)),
        nullable=nullable, **kwargs)


class TimestampMixin(object):
    created = Column(
        db.DateTime, nullable=False, default=datetime.datetime.utcnow)
        
    updated = Column(db.DateTime, onupdate=datetime.datetime.utcnow)


class GUID(TypeDecorator):
    """Platform-independent GUID type.

    Uses Postgresql's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.

    """
    impl = CHAR

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == 'postgresql':
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            return uuid.UUID(value)


class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        current_app.logger.debug('to_collection_dict called')
        data = {
            '_links': {},
            '_meta': {},
            'items': []
        }
        record_count = len(query.all())
        current_app.logger.debug('found {} records in to_collection_dict'.format(record_count))
        if record_count == 0:
            return data
        elif record_count <= per_page:
            data['items'] = [item.to_dict() for item in query.all()]
            return data

        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs),
                'next': url_for(endpoint, page=page + 1, per_page=per_page,
                                **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, per_page=per_page,
                                **kwargs) if resources.has_prev else None
            }
        }
        return data

##############################################################
# # Activity Streams
##############################################################


class ActivityLog(TimestampMixin, SurrogatePK, Model):
    __tablename__ = 'activity_log'
    player_id = reference_col('players', nullable=True)
    description = Column(db.Text, nullable=False)
    admin_event = Column(db.Boolean, default=False, nullable=False)

    def __init__(self, player_id=None, description='', **kwargs):
        db.Model.__init__(self, player_id=player_id, 
                          description=description, **kwargs)


def record_activity(player_id, message, activity_context=None, 
                    admin_event=False):
    try:
        if activity_context is not None:
            message = message + ' from {}'.format(activity_context)
        new_log = ActivityLog(player_id, message, admin_event=admin_event)
        new_log.save()
        return True
    except Exception as e:
        print("There was an error saving a log message: {}".format(str(e)))
        return False
##############################################################
# Mob Tables
##############################################################


class Mob(SurrogatePK, Model, TimestampMixin, PaginatedAPIMixin):

    __tablename__ = 'mobs'
    topic = Column(db.String(128), nullable=False)
    location = Column(db.String(128), nullable=True)
    start_time = Column(db.DateTime)
    end_time = Column(db.DateTime)
    time_running = Column(db.Boolean, default=False)
    active = Column(db.Boolean, default=False)
    interval_length = Column(db.Integer, default=7)
    team_id = reference_col('teams')
    sound_id = reference_col('sounds', nullable=True)
    # players = relationship('Player', 
    #                        secondary='mob_player_associations',
    #                        backref=backref('mobs', uselist=False),
    #                        lazy='dynamic')
    players = relationship('Player',
                           secondary='mob_player_associations',
                           back_populates='mobs',
                           lazy='dynamic')

    def __init__(self, topic, location, team_id, **kwargs):
        db.Model.__init__(self, topic=topic, 
                          location=location, team_id=team_id, **kwargs)
        self.update(active=True)

    def __repr__(self):
        if self.active:
            return '<Mob {} covering {} in {}>'\
                .format(self.id, self.topic, self.location)
        else:
            return '<Mob {} currently inactive>'\
                .format(self.id)

    default_fields = [
        'id',
        'topic',
        'location'
    ]
    read_only_fields = [
        'id'
    ]

    def to_dict(self, **kwargs):
        data = super(Mob, self).to_dict(**kwargs)
        data['_links'] = {
            'self': url_for('api.mob', id=self.id),
            'players': ''
        }
        return data

    @validates('topic', 'location')
    def validate_strings(self, key, field):
        if not field:
            raise AssertionError('Must provide topic and location strings')

        if type(field) is not str:
            current_app.logger.error('Mob topic and location must be strings, got {}'.format(type(field)))
            raise AssertionError('Mob topic and location must be strings, got {}'.format(type(field)))

        return field

    def start_timer(self):
        # first check to see if timer was ended >30min prior
        # if so, expire the mob
        if self.end_time:
            time_limit = datetime.timedelta(minutes=30)
            if datetime.datetime.utcnow() > (self.end_time + time_limit):
                print("Disbanding mob {} for being inacive over 30 minutes".format(self.id))
                self.end_mob()

        if self.time_running:
            # timer is running, so someone clicked restart
            # at the end of an interval
            self.stop_timer()
            self.rotate_queue()
        self.update(start_time=datetime.datetime.utcnow())
        self.update(end_time=None)
        self.update(time_running=True)
        return True

    def stop_timer(self):
        # now = datetime.datetime.utcnow()
        # if now > (self.start_time + datetime.timedelta(minutes=self.interval_length)):
        #     print("""Mob {} started at {} with an interval of {}min -
        #     however it has been {} minutes since the timer started.
        #     Only giving credit for minutes in interval legnth"""
        #           .format(self.id,
        #                   self.start_time.strftime('%H:%M'),
        #                   self.interval_length,
        #                   (now - self.start_time) / 60)
        #           )
        #     self.end_time = self.start_time + datetime.timedelta(minutes=self.interval_length)
        # else:
        self.update(end_time=datetime.datetime.utcnow())
        self.update(time_running=False)
        time_diff = self.end_time - self.start_time
        minutes_earned = math.floor(time_diff.seconds / 60)
        for p in self.players:
            p.log_minutes(int(minutes_earned))
        return True

    def end_mob(self):
        if not self.end_time and self.start_time:
            self.stop_timer()
        self.update(active=False)
        self.update(time_running=False)
        for p in self.players:
            p.queue_position = 0
        return True

    def add_player(self, player_id):
        player = Player.get_by_id(player_id)
        if player and player.team_id == self.team_id:
            try:
                self.players.append(player)
                return True
            except Exception as e:
                print str(e)
                return False
        else:
            print 'Could not add {} to mob {}'.format(player.username, self.id)

        return False

    def rotate_queue(self):
        try:
            player_count = len(self.players.all())
            for player in self.players:
                print('Updating queue position for {}'.format(player.username))
                if player.queue_position == 1:
                    player.update(queue_position=player_count)
                    print('moving {} to the end of the queue'.format(player.username))
                else:
                    player.update(queue_position=player.queue_position-1)
                print('Player {} is now place {} in mob queue'.format(player.username, player.queue_position))
            return True
        except Exception as e:
            print('Error rotating mob: {}'.format(str(e)))
            return False

##############################################################
# Player Tables
##############################################################


class Team(SurrogatePK, Model, PaginatedAPIMixin, TimestampMixin):

    __tablename__ = 'teams'
    name = Column(db.String(128), nullable=False)
    default_interval_length = Column(db.Integer, default=15, nullable=False)

    players = relationship('Player',
                           backref=backref('team', uselist=False),
                           lazy='dynamic')
    mobs = relationship('Mob',
                        backref=backref('team', uselist=False),
                        lazy='dynamic')

    default_fields = [
        'id',
        'name',
        'default_interval_length'
    ]
    read_only_fields = [
        'id',
        'created'
    ]
    def to_dict(self, **kwargs):
        data = super(Team, self).to_dict(**kwargs)
        data['_links'] = {
            'self': url_for('api.team', id=self.id),
            'players': '',
            'mobs': ''
        }
        return data

    @validates('name')
    def validate_team_name(self, key, name):
        if not name:
            raise AssertionError('Must provide a team name')

        if type(name) is not str:
            current_app.logger.error('Team name should be string, instead found {}'.format(type(name)))
            raise AssertionError('Name must be a string')

        if Team.query.filter(Team.name==name).first():
            raise AssertionError('Team name is already in use')

        if len(name) < 4 or len(name) > 20:
            raise AssertionError('Team name must be between 4-20 characters')

        return name


class Player(SurrogatePK, Model, PaginatedAPIMixin, TimestampMixin):

    __tablename__ = 'players'
    username = Column(db.String(128), nullable=False)
    email = Column(db.String(120), unique=True)
    level = Column(db.Integer, default=1)
    xp = Column(db.Integer, default=0)
    all_time_xp = Column(db.Integer, default=0)
    team_admin = Column(db.Boolean, default=False, nullable=False)
    queue_position = Column(db.Integer, nullable=True)
    gitlab_username = Column(db.String(128))
    class_id = reference_col('player_classes')
    team_id = reference_col('teams')

    # player registration
    confirmation_token = Column(db.String(256))
    confirmed = Column(db.Boolean, default=False, nullable=False)
    confirmed_on = Column(db.DateTime, nullable=True)

    # auth fields
    password_hash = Column(db.String(128))
    access_token = Column(db.String(512), unique=True)
    refresh_token = Column(db.String(512), unique=True)

    # serialization settings
    default_fields = [
        'id',
        'username',
        'email',
        'level',
        'xp',
        'team_admin',
        'class_id',
        'team_id'
    ]
    hidden_fields = [
        'password_hash'
    ]
    read_only_fields = [
        'password_hash'
    ]

    # relationships
    mobs = relationship('Mob', 
                        secondary='mob_player_associations',
                        back_populates='players',
                        lazy='dynamic')
    sent_gifts = relationship('Gift', backref="gift_sender")
    received_gifts = relationship('Gift', 
                                  secondary='gift_recipient_associations',
                                  back_populates='players')
    bounties = relationship('Bounty',
                            secondary='bounty_player_associations',
                            back_populates='players')
    coins = relationship('Coin', backref='player')
    activity = relationship('ActivityLog', backref='player')

    def __init__(self, username, password, email, team_id, **kwargs):
        db.Model.__init__(self, username=username,
                          class_id=1,
                          email=email,
                          password_hash=self.create_password_hash(password),
                          team_id=team_id,
                          **kwargs)
        # self.create_tokens()
        # self.confirmation_token = self.generate_confirmation_token()

    ##################################################
    # Field validation
    ##################################################

    @validates('username')
    def validate_username(self, key, username):
        if not username:
            raise AssertionError('Must provide a username')

        if type(username) is not str:
            current_app.logger.error('Username should be string, instead found {}'.format(type(username)))
            raise AssertionError('Username must be a string')

        if Player.query.filter(Player.username==username).first():
            raise AssertionError('Username is already in use')

        if len(username) < 4 or len(username) > 20:
            raise AssertionError('Username must be between 4-20 characters')

        return username

    @validates('email')
    def validate_email(self, key, email):
        if not email:
            raise AssertionError('Must provide a email')

        if type(email) is not str:
            raise AssertionError('Email must be a string')

        if not re.match("[^@]+@[^@]+\.[^@]+", email):
            raise AssertionError('Provided email is not a valid email address')

        if Player.query.filter(Player.email==email).first():
            raise AssertionError('Email address already in use')

        return email

    ##################################################
    # end validation methods
    ##################################################

    def generate_confirmation_token(self):
        serializer = URLSafeTimedSerializer(app_config.SECRET_KEY)
        return serializer.dumps(self.email, 
                                salt=app_config.SECURITY_PASSWORD_SALT)

    def confirm_token(self, token, expiration=3600):
        serializer = URLSafeTimedSerializer(app_config.SECRET_KEY)
        try:
            email = serializer.loads(
                token,
                salt=app_config['SECURITY_PASSWORD_SALT'],
                max_age=expiration
            )
        except:
            return False
        return email

    # def create_tokens(self, refresh=True):
    #     self.access_token = self._create_access_token()
    #     if refresh:
    #         self.refresh_token = self._create_refresh_token()

    # def _create_access_token(self):
    #     return create_access_token(identity=self.email)

    # def _create_refresh_token(self):
    #     return create_refresh_token(identity=self.email)

    def to_dict(self, with_tokens=False, **kwargs):
        data = super(Player, self).to_dict(**kwargs)
        data['_links'] = {
                'self': url_for('api.player', id=self.id),
                'team': '',
                'bounties': '',
                'mobs': '',
                'gifts_received': '',
                'gifts_sent': '',
                'bounties': '',
                'coins': '',
                'events': ''
        }

        if with_tokens:
            data['access_token'] = self.access_token
            data['refresh_token'] = self.refresh_token

        return data

    @property
    def in_mob(self):
        active_mobs = [m for m in self.mobs if m.active is True]
        if len(active_mobs) > 0:
            return True
        return False


    @classmethod
    def get_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    @classmethod
    def get_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    def create_password_hash(self, password):
        if not password:
            raise AssertionError('Password must be provided')

        if not re.match('\d.*[A-Z]|[A-Z].*\d', password):
            raise AssertionError('Password must contain 1 capital letter and 1 number')

        if len(password) < 8 or len(password) > 50:
            raise AssertionError('Password must be between 8 and 50 characters')

        # not specifying the method here made this method take literally 10x longer
        return generate_password_hash(password, method='pbkdf2:sha256:200')

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def check_token(token):
        player = Player.query.filter_by(token=token).first()
        if player is None or player.token_expiration < datetime.datetime.utcnow():
            return None
        return player

    def log_minutes(self, count, activity_context=None):
        print("Logging {} minutes for {}".format(count, self.username))
        record_activity(self.id,
                        "{} logged {} minutes".format(self.username, count),
                        activity_context=activity_context)
        self.update_xp(count)

    def level_up(self):
        self.level += 1
        print("### {} has leveled up to {}! ###".format(self.username,
                                                        self.level))
        record_activity(self.id, 
                        "{} has reached level {}!".format(self.username,
                                                          self.level))
        self.xp = 0
        new_coin = Coin(player_id=self.id)
        self.coins.append(new_coin)
        print("Awarded coin {} to {} for levelling up"
              .format(new_coin.guid.fields[0], self.username))

    def update_xp(self, new_xp):
        """Take in a new amount of XP. Apply in chunks so that we can account
        for each level up"""
        while new_xp > 0:
            next_lvl_xp = LevelExpMap.get_required_xp(self.level)
            if (self.xp + new_xp) >= next_lvl_xp:
                lvl_xp_diff = next_lvl_xp - self.xp
                self.level_up()
                self.all_time_xp += lvl_xp_diff
                new_xp -= lvl_xp_diff
            else:
                print("Adding {}XP to {}".format(new_xp, self.username))
                self.xp += new_xp
                record_activity(self.id, "{} gained {}XP".format(self.username, new_xp))
                self.all_time_xp += new_xp
                new_xp = 0

            self.save()

        print("Finished updating {}'s XP".format(self.username))


class RevokedTokenModel(SurrogatePK, db.Model):
    __tablename__ = 'revoked_tokens'
    jti = Column(db.String(120))
    
    def add(self):
        db.session.add(self)
        db.session.commit()
    
    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti = jti).first()
        return bool(query)


class PlayerClass(SurrogatePK, Model):

    __tablename__ = 'player_classes'
    name = Column(db.String(128), nullable=False, default='ClassName')
    description = Column(db.Text, nullable=False, default='Class Description')
    players = relationship('Player', backref='player_class')
    pic_id = reference_col('images', nullable=True)

    def __init__(self, name, **kwargs):
        db.Model.__init__(self, name=name, **kwargs)


class LevelExpMap(SurrogatePK, Model):

    __tablename__ = 'lvl_xp_map'
    level = Column(db.Integer, unique=True)
    req_xp = Column(db.Integer, default=1800)

    @classmethod
    def get_required_xp(cls, current_level):
        """Traverse all recorded XP gates. Once we hit a lvl
        higher than current_lvl, return that req_xp
        """
        for record in cls.query.order_by(cls.level.asc()).all():
            if current_level < record.level:
                return record.req_xp


###################################
# Economy Models
###################################

class Bounty(SurrogatePK, TimestampMixin, Model):
    __tablename__ = 'bounties'
    name = Column(db.String(256), nullable=False, default="Default Bounty")
    description = Column(db.Text, nullable=False, default="Bounty description")
    project = Column(db.String(128))
    project_url = Column(db.String(128))
    xp_reward = Column(db.Integer, nullable=False, default=0)
    issue_id = Column(db.Integer)
    code_commit = Column(db.String(256))
    note_url = Column(db.String(256))
    pic = Column(db.String(128), default='/static/img/treasurchest.png', 
                 nullable=True)
    players = relationship('Player',
                           secondary='bounty_player_associations',
                           back_populates='bounties')


class Coin(SurrogatePK, Model, TimestampMixin):
    __tablename__ = 'coins'
    guid = Column(GUID)
    picture = Column(db.String(64), default='/static/img/coin.png')
    player_id = reference_col('players')
    is_spent = Column(db.Boolean, default=False, nullable=False)
    spent_on = Column(db.DateTime, default=datetime.datetime.now)
    spent_reason = Column(db.Text)
    gift_id = reference_col('gifts', nullable=True)

    def __init__(self, player_id, **kwargs):
        db.Model.__init__(self, player_id=player_id, **kwargs)
        self.guid = uuid.uuid4()


class Gift(SurrogatePK, Model, TimestampMixin):
    __tablename__ = 'gifts'
    name = Column(db.String(128), nullable=False, default="Default Gift")
    description = Column(db.Text, nullable=False, default="Gift Description")
    xp_value = Column(db.Integer, nullable=False, default=0)
    sender_id = reference_col('players')
    players = relationship('Player',
                           secondary='gift_recipient_associations',
                           back_populates='received_gifts')


class ImageResource(SurrogatePK, Model):
    __tablename__ = 'images'
    path = Column(db.String(128))
    classes = relationship('PlayerClass', backref="image")


class AudioResource(SurrogatePK, Model):
    __tablename__ = 'sounds'
    path = Column(db.String(128))
    mobs = relationship('Mob', backref="alert_sound")


###################################
# Many to Many Association Tables
###################################

class GiftRecipientAssociations(Model):
    __tablename__ = 'gift_recipient_associations'
    player_id = Column(db.Integer,
                       ForeignKey('players.id'),
                       primary_key=True)
    gift_id = Column(db.Integer,
                     ForeignKey('gifts.id'),
                     primary_key=True)


class BountyPlayerAssociations(Model):
    __tablename__ = 'bounty_player_associations'
    player_id = Column(db.Integer, ForeignKey('players.id'), primary_key=True)
    bounty_id = Column(db.Integer, ForeignKey('bounties.id'), primary_key=True)
    bounty_completed = Column(db.Boolean, default=True, nullable=False)
    completed_on = Column(db.DateTime, default=func.now(), nullable=True)


class MobPlayerAssociations(Model):
    __tablename__ = 'mob_player_associations'
    player_id = Column(db.Integer, ForeignKey('players.id'), primary_key=True)
    mob_id = Column(db.Integer, ForeignKey('mobs.id'), primary_key=True)
    time_added = Column(db.DateTime, nullable=True, default=func.now())