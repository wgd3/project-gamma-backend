import os
import click
from flask import current_app
from flask.cli import with_appcontext
from werkzeug.exceptions import MethodNotAllowed, NotFound
from faker import Faker

HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)
TEST_PATH = os.path.join(PROJECT_ROOT, 'tests')


@click.command()
def test():
    """Run the tests."""
    import pytest
    rv = pytest.main([TEST_PATH,
                      '--verbose',
                      '--log-level=DEBUG',
                      '-s'])
    exit(rv)


@click.command()
@with_appcontext
def add_team():
    from gamma.models import Team
    print('\nAdding new team...')
    fakeData = Faker()
    team_data = {
        'name': 'Team {}'.format(fakeData.domain_word()),
        'default_interval_length': fakeData.random_int(min=5, max=15)
    }
    try:
        team = Team.create(**team_data)
        print('{} created successfully!'.format(team.name))
    except Exception as e:
        print('Error saving new team:\n{}'.format(e.message))


@click.command()
@click.option('--count', default=1)
@click.option('--team', default=1)
@click.option('--admin', default=False)
@with_appcontext
def add_player(count, team, admin):
    from gamma.models import Player
    print('\nAdding new players...')
    fakeData = Faker()
    newPlayers = []
    while count > 0:
        player_data = {
            'username': '{}'.format(fakeData.user_name()),
            'email': '{}'.format(fakeData.email()),
            'password': 'Password1',
            'team_id': team,
            'team_admin': admin
        }
        try:
            player = Player.create(**player_data)
            newPlayers.append(player)
        except Exception as e:
            print('Error saving new player:\n{}'.format(e.message))

        count -= 1

    if len(newPlayers) > 0:
        print('New Players:')
        for p in newPlayers:
            print(p.username)


@click.command()
def coverage():
    """Run the tests."""
    from coverage import coverage
    import pytest
    cov = coverage(config_file=True)
    cov.start()
    rv = pytest.main([TEST_PATH,
                      '--verbose',
                      '--log-level=DEBUG',
                      '-s'])
    cov.stop()
    cov.save()
    print("\n\nCoverage Report:\n")
    cov.report()
    print("\nHTML version: " + os.path.join(PROJECT_ROOT, "htmlcov/index.html"))
    cov.html_report(directory='htmlcov')
    cov.erase()


@click.command()
def clean():
    """Remove *.pyc and *.pyo files recursively starting at current directory.
    Borrowed from Flask-Script, converted to use Click.
    """
    for dirpath, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            if filename.endswith('.pyc') or filename.endswith('.pyo'):
                full_pathname = os.path.join(dirpath, filename)
                click.echo('Removing {}'.format(full_pathname))
                os.remove(full_pathname)


@click.command()
@click.option('--url', default=None,
              help='Url to test (ex. /static/image.png)')
@click.option('--order', default='rule',
              help='Property on Rule to order by (default: rule)')
@with_appcontext
def urls(url, order):
    """Display all of the url matching routes for the project.
    Borrowed from Flask-Script, converted to use Click.
    """
    rows = []
    column_length = 0
    column_headers = ('Rule', 'Endpoint', 'Arguments')

    if url:
        try:
            rule, arguments = (
                current_app.url_map
                           .bind('localhost')
                           .match(url, return_rule=True))
            rows.append((rule.rule, rule.endpoint, arguments))
            column_length = 3
        except (NotFound, MethodNotAllowed) as e:
            rows.append(('<{}>'.format(e), None, None))
            column_length = 1
    else:
        rules = sorted(
            current_app.url_map.iter_rules(),
            key=lambda rule: getattr(rule, order))
        for rule in rules:
            rows.append((rule.rule, rule.endpoint, None))
        column_length = 2

    str_template = ''
    table_width = 0

    if column_length >= 1:
        max_rule_length = max(len(r[0]) for r in rows)
        max_rule_length = max_rule_length if max_rule_length > 4 else 4
        str_template += '{:' + str(max_rule_length) + '}'
        table_width += max_rule_length

    if column_length >= 2:
        max_endpoint_length = max(len(str(r[1])) for r in rows)
        # max_endpoint_length = max(rows, key=len)
        max_endpoint_length = (
            max_endpoint_length if max_endpoint_length > 8 else 8)
        str_template += '  {:' + str(max_endpoint_length) + '}'
        table_width += 2 + max_endpoint_length

    if column_length >= 3:
        max_arguments_length = max(len(str(r[2])) for r in rows)
        max_arguments_length = (
            max_arguments_length if max_arguments_length > 9 else 9)
        str_template += '  {:' + str(max_arguments_length) + '}'
        table_width += 2 + max_arguments_length

    click.echo(str_template.format(*column_headers[:column_length]))
    click.echo('-' * table_width)

    for row in rows:
        click.echo(str_template.format(*row[:column_length]))