from flask import current_app, make_response, jsonify, request
from flask_restful import Resource, Api, reqparse
from flask_restful_swagger import swagger
from flask_jwt_extended import create_access_token, create_refresh_token, \
                               jwt_required, jwt_refresh_token_required, \
                               get_jwt_identity, get_raw_jwt

from . import api_bp
from gamma.app import jwt
from gamma.models import Player, Team, Mob, PlayerClass, \
                         Bounty, Coin, Gift, ImageResource as Image, \
                         AudioResource as Audio, LevelExpMap, \
                         RevokedTokenModel
from .errors import bad_request, error_response

api = swagger.docs(Api(api_bp), apiVersion='1.0',
                   api_spec_url='/spec')


def convert_uni_to_str(data):
    """
    API requests that come in are in unicode format,
    that needs to be converted to strings
    :param data:
    :return: converted data
    """
    for k, v in data.iteritems():
        if type(data.get(k, None)) == unicode:
            data[k] = str(data[k])
    return data


@jwt.expired_token_loader
def my_expired_token_callback():
    resp = jsonify({
        'msg': 'The token has expired'
    })
    resp.status_code = 401
    return resp

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)

class TokenRefresh(Resource):
    @swagger.operation(
        notes='Issues new access token from refresh token'
    )
    @jwt_refresh_token_required
    def post(self):
        current_user_email = get_jwt_identity()
        current_user = Player.get_by_email(current_user_email)
        new_access_token = create_access_token(identity=current_user.email,
                                                   fresh=False)
        current_user.update(access_token=new_access_token)
        return {'access_token': new_access_token}


class PlayerLogoutAccess(Resource):
    @swagger.operation(
        notes='Revoke player access token'
    )
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class PlayerLogoutRefresh(Resource):
    @swagger.operation(
        notes='Revoke player refresh token'
    )
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class GetAuthedPlayer(Resource):
    @swagger.operation(
        notes='Get player details for currently logged in player'
    )
    @jwt_required
    def get(self):
        current_user_email = get_jwt_identity()
        current_user = Player.get_by_email(current_user_email)
        if current_user:
            return current_user.to_dict(show=[
                'id',
                'username',
                'email',
                'team_id',
                'level',
                'xp'
            ])
        else:
            return error_response(500, 'Player could not be found')

class PlayerResource(Resource):
    """Player API"""

    @swagger.operation(
        notes='Returns a player by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the player",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found player"
            },
            {
                "code": 404,
                "message": "Player not found"
            }
        ]
    )
    def get(self, id):
        return Player.query.get_or_404(id).to_dict()

    @swagger.operation(
        notes='Deletes a player by id',
        nickname='delete',
        parameters=[
            {
                "name": "id",
                "description": "The ID of the player",
                "required": True,
                "allowMultiple": False,
                "dataType": "integer",
                "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Deleted player"
            },
            {
                "code": 404,
                "message": "Player not found"
            },
            {
                "code": 500,
                "message": "Database error"
            }
        ]
    )
    def delete(self, id):
        player = Player.query.get_or_404(id)
        try:
            player.delete()
            resp = jsonify({'message': 'Deleted player'})
            resp.status_code = 200
        except Exception as e:
            current_app.logger.error('There was an error deleting player {}: {}'.format(id, e.message))
            resp = error_response(500, e.message)

        return resp

    @swagger.operation(
                notes='Updates a player by id',
                nickname='put',
                parameters=[
                    {
                      "name": "id",
                      "description": "The ID of the player",
                      "required": True,
                      "allowMultiple": False,
                      "dataType": "integer",
                      "paramType": "path"
                    },
                    {
                        "name": "data",
                        "description": "New player data",
                        "required": True,
                        "allowMultiple": False,
                        "dataType": "string",
                        "paramType": "body"
                    }
                ],
                responseMessages=[
                    {
                        "code": 200,
                        "message": "Updated player"
                    },
                    {
                        "code": 404,
                        "message": "Player not found"
                    },
                    {
                        "code": 500,
                        "message": "Database error"
                    }
                ]
    )
    def put(self, id):
        player = Player.query.get_or_404(id)
        data = request.get_json(force=True) or {}
        converted_data = convert_uni_to_str(data)
        try:
            changes = player.set_columns(**converted_data)
            player.save()
            resp = jsonify({'changes': changes})
            resp.status_code = 200
            current_app.logger.info('Updated the following fields for player {}: {}'
                                    .format(id, changes))
        except AssertionError as err:
            current_app.logger.error('AssertionError when updating player: {}'.format(err.message))
            resp = bad_request(err.message)
        except Exception as err:
            current_app.logger.error('Error when updating player: {}'.format(err.message))
            resp = bad_request(err.message)

        return resp

class PlayerListResource(Resource):
    """Player List API"""

    def __init__(self):
        self.req_parser = reqparse.RequestParser()
        self.req_parser.add_argument('data', type=dict, location='json')
        # super(PlayerListResource, self).__init__()

    @swagger.operation(
        notes='Returns all players',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found players"
            },
            {
                "code": 404,
                "message": "Players not found"
            }
        ]
    )
    def get(self):
        page = request.args.get('page', 1, type=int)
        per_page = min(request.args.get('per_page', 30, type=int), 100)
        data = Player.to_collection_dict(Player.query, page, per_page, 'api.players')
        current_app.logger.debug('Returning {} players'.format(len(data['items'])))
        return data, 200

    @swagger.operation(
        notes='Creates a new player',
        nickname='post',
        parameters=[
            {
                "name": "data",
                "description": "New player data",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 201,
                "message": "Created new player"
            },
            {
                "code": 400,
                "message": "Player could not be created from provided information"
            }
        ]
    )
    def post(self):
        """
        Creates a new player
        """
        player_data = request.get_json()['data'] or {}
        current_app.logger.debug('Found player data: {}'.format(player_data))
        if 'username' not in player_data or \
            'email' not in player_data or \
            'password' not in player_data or \
            'team_id' not in player_data:
            return bad_request('Request must include username, password, email, and team_id')

        converted_data = convert_uni_to_str(player_data)
        try:
            player = Player.create(**converted_data)
            new_access_token = create_access_token(identity=player.email,
                                                   fresh=True)
            new_refresh_token = create_refresh_token(identity=player.email)
            player.update(access_token=new_access_token,
                          refresh_token=new_refresh_token)
            resp = jsonify(player.to_dict(show=['access_token', 'refresh_token']))
            resp.status_code = 201
            current_app.logger.info('Created new player {}!'.format(player.username))

        except AssertionError as err:
            current_app.logger.error('AssertionError when creating new player: {}'.format(err.message))
            resp = bad_request(err.message)
        except Exception as err:
            current_app.logger.error('Error when creating new player: {}'.format(err.message))
            resp = bad_request(err.message)

        return resp


class PlayerLoginResource(Resource):
    """
    Player login API
    https://codeburst.io/jwt-authorization-in-flask-c63c1acf4eeb
    """
    def __init__(self):
        self.req_parser = reqparse.RequestParser()
        self.req_parser.add_argument('email', required=True, help='Email is required to login')
        self.req_parser.add_argument('password', required=True, help='Password is required to login')
        super(PlayerLoginResource, self).__init__()

    def post(self):
        data = self.req_parser.parse_args()
        current_user = Player.get_by_email(data['email'])
        if not current_user:
            return bad_request('Bad email or password')
        if current_user.check_password(data['password']):
            new_access_token = create_access_token(identity=current_user.email,
                                                   fresh=True)
            new_refresh_token = create_refresh_token(identity=current_user.email)
            current_user.update(access_token=new_access_token,
                                refresh_token=new_refresh_token)                                  
            return jsonify({
                'message': 'Logged in!',
                'access_token': new_access_token,
                'refresh_token': new_refresh_token
            })
        else:
            return bad_request('Bad email or password')


class TeamResource(Resource):
    """Team API"""
    @swagger.operation(
        notes='Returns a team by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the team",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found team"
            },
            {
                "code": 404,
                "message": "Team not found"
            }
        ]
    )
    def get(self, id):
        return Team.query.get_or_404(id).to_dict()

    @swagger.operation(
        notes='Deletes a team'
    )
    def delete(self, id):
        team = Team.query.get_or_404(id)
        try:
            team.delete()
            resp = jsonify({'message': 'Deleted team'})
            resp.status_code = 200
        except Exception as e:
            current_app.logger.error('There was an error deleting team {}: {}'.format(id, e.message))
            resp = error_response(500, e.message)
        
        return resp

    @swagger.operation(
        notes='Update a teams properties',
        parameters=[
            {
                "name": "id",
                "description": "The ID of the team",
                "required": True,
                "allowMultiple": False,
                "dataType": "integer",
                "paramType": "path"
            },
            {
                "name": "data",
                "description": "Team data",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Updated team"
            },
            {
                "code": 404,
                "message": "Team not found"
            },
            {
                "code": 500,
                "message": "Database error"
            }
        ]
    )
    def put(self, id):
        team = Team.query.get_or_404(id)
        data = request.get_json(force=True) or {}
        converted_data = convert_uni_to_str(data)
        try:
            changes = team.set_columns(**converted_data)
            team.save()
            resp = jsonify({'changes': changes})
            resp.status_code = 200
            current_app.logger.info('Updated the following fields for team {}: {}'
                                    .format(id, changes))
        except AssertionError as err:
            current_app.logger.error('AssertionError when updating team: {}'.format(err.message))
            resp = bad_request(err.message)
        except Exception as err:
            current_app.logger.error('Error when updating team: {}'.format(err.message))
            resp = bad_request(err.message)

        return resp
        

class TeamListResource(Resource):
    """Team List API"""
    @swagger.operation(
        notes='Returns all teams',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found players"
            },
            {
                "code": 404,
                "message": "Players not found"
            }
        ]
    )
    def get(self):
        page = request.args.get('page', 1, type=int)
        per_page = min(request.args.get('per_page', 10, type=int), 100)
        data = Team.to_collection_dict(Team.query, page, per_page, 'api.teams')
        return data, 200

    @swagger.operation(
        notes='Creates a new team'
    )
    def post(self):
        """Creates a new team"""
        team_data = request.get_json()['data'] or {}
        current_app.logger.debug('Found team data: {}'.format(team_data))
        if 'name' not in team_data:
            return bad_request('Request must include team name')

        converted_data = convert_uni_to_str(team_data)
        try:
            team = Team.create(**converted_data)
            resp = jsonify(team.to_dict())
            resp.status_code = 201
            current_app.logger.info('Created new team with ID {}'.format(team.id))
        except AssertionError as err:
            current_app.logger.error('AssertionError when creating new player: {}'.format(err.message))
            resp = bad_request(err.message)
        except Exception as err:
            current_app.logger.error('Error when creating new player: {}'.format(err.message))
            resp = bad_request(err.message)
        
        return resp


class MobResource(Resource):
    """Mob API"""
    @swagger.operation(
        notes='Returns a mob by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the mob",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found mob"
            },
            {
                "code": 404,
                "message": "Mob not found"
            }
        ]
    )
    def get(self, id):
        return Mob.query.get_or_404(id).to_dict()

    @swagger.operation(
        notes='Deletes a mob'
    )
    def delete(self, id):
        mob = Mob.query.get_or_404(id)
        try:
            mob.delete()
            resp = jsonify({'message': 'Deleted mob'})
            resp.status_code = 200
        except Exception as e:
            current_app.logger.error('There was an error deleting mob {}: {}'.format(id, e.message))
            resp = error_response(500, e.message)
        
        return resp

    @swagger.operation(
        notes='Update a mob',
        parameters=[
            {
                "name": "id",
                "description": "The ID of the mob",
                "required": True,
                "allowMultiple": False,
                "dataType": "integer",
                "paramType": "path"
            },
            {
                "name": "data",
                "description": "Mob data",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Updated mob"
            },
            {
                "code": 404,
                "message": "Mob not found"
            },
            {
                "code": 500,
                "message": "Database error"
            }
        ]
    )
    def put(self, id):
        mob = Mob.query.get_or_404(id)
        data = request.get_json(force=True) or {}
        converted_data = convert_uni_to_str(data)
        try:
            changes = mob.set_columns(**converted_data)
            mob.save()
            resp = jsonify({'changes': changes})
            resp.status_code = 200
            current_app.logger.info('Updated the following fields for mob {}: {}'
                                    .format(id, changes))
        except AssertionError as err:
            current_app.logger.error('AssertionError when updating mob: {}'.format(err.message))
            resp = bad_request(err.message)
        except Exception as err:
            current_app.logger.error('Error when updating mob: {}'.format(err.message))
            resp = bad_request(err.message)

        return resp
        


class MobListResource(Resource):
    """Mob List API"""

    def __init__(self):
        self.req_parser = reqparse.RequestParser()
        self.req_parser.add_argument('data', type=dict, location='json')

    @swagger.operation(
        notes='Returns all mobs',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found mobs"
            }
        ]
    )
    def get(self):
        page = request.args.get('page', 1, type=int)
        per_page = min(request.args.get('per_page', 10, type=int), 100)
        data = Mob.to_collection_dict(Mob.query, page, per_page, 'api.mobs')
        return data, 200

    @swagger.operation(
        notes='Creates a new mob',
        parameters=[
            {
                "name": "data",
                "description": "New mob data",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 201,
                "message": "Created new mob"
            },
            {
                "code": 400,
                "message": "mob could not be created from provided information"
            }
        ]
    )
    def post(self):
        """Creates a new mob"""
        mob_data = request.get_json() or {}
        current_app.logger.debug('Found mob data: {}'.format(mob_data))
        if 'topic' not in mob_data or \
           'location' not in mob_data or \
           'team_id' not in mob_data:
            return bad_request('Request must include mob topic, location, and team id')

        converted_data = convert_uni_to_str(mob_data)
        try:
            mob = Mob.create(**converted_data)
            resp = jsonify(mob.to_dict())
            resp.status_code = 201
            current_app.logger.info('Created new mob with ID {}'.format(mob.id))
        except AssertionError as err:
            current_app.logger.error('AssertionError when creating new mob: {}'.format(err.message))
            resp = bad_request(err.message)
        except Exception as err:
            current_app.logger.error('Error when creating new mob: {}'.format(err.message))
            resp = bad_request(err.message)
        
        return resp


class PlayerClassResource(Resource):
    """Player Class API"""
    @swagger.operation(
        notes='Returns a player class by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the player class",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found player class"
            },
            {
                "code": 404,
                "message": "Player class not found"
            }
        ]
    )
    def get(self, id):
        pc = PlayerClass.get_by_id(id)
        # return PlayerClassSchema().jsonify(pc)


class PlayerClassListResource(Resource):
    """Player Class List API"""
    @swagger.operation(
        notes='Returns all player classes',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found player classes"
            },
            {
                "code": 404,
                "message": "Player classes not found"
            }
        ]
    )
    def get(self):
        pcs = PlayerClass.query.all()
        # return PlayerClassSchema(many=True).jsonify(pcs)


class BountyResource(Resource):
    """Bounty Class API"""
    @swagger.operation(
        notes='Returns a bounty by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the bounty",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found bounty"
            },
            {
                "code": 404,
                "message": "Bounty not found"
            }
        ]
    )
    def get(self, id):
        bounty = Bounty.get_by_id(id)
        # return BountySchema().jsonify(bounty)


class BountyListResource(Resource):
    """Bounty List API"""
    @swagger.operation(
        notes='Returns all bounties',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found bounties"
            },
            {
                "code": 404,
                "message": "Bounties not found"
            }
        ]
    )
    def get(self):
        bounties = Bounty.query.all()
        # return BountySchema(many=True).jsonify(bounties)


class CoinResource(Resource):
    """Coin Class API"""
    @swagger.operation(
        notes='Returns a coin by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the coin",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found coin"
            },
            {
                "code": 404,
                "message": "Coin not found"
            }
        ]
    )
    def get(self, id):
        coin = Coin.get_by_id(id)
        # return CoinSchema().jsonify(coin)


class CoinListResource(Resource):
    """Coin List API"""
    @swagger.operation(
        notes='Returns all coins',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found coins"
            },
            {
                "code": 404,
                "message": "Coins not found"
            }
        ]
    )
    def get(self):
        coins = Coin.query.all()
        # return CoinSchema(many=True).jsonify(coins)


class GiftResource(Resource):
    """Gift Class API"""
    @swagger.operation(
        notes='Returns a gift by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the gift",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found gift"
            },
            {
                "code": 404,
                "message": "Gift not found"
            }
        ]
    )
    def get(self, id):
        gift = Gift.get_by_id(id)
        # return GiftSchema().jsonify(gift)


class GiftListResource(Resource):
    """Gift List API"""
    @swagger.operation(
        notes='Returns all gifts',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found gifts"
            },
            {
                "code": 404,
                "message": "Gifts not found"
            }
        ]
    )
    def get(self):
        gifts = Gift.query.all()
        # return GiftSchema(many=True).jsonify(gifts)


class ImageResource(Resource):
    """Image Class API"""
    @swagger.operation(
        notes='Returns an image by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the image",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found image"
            },
            {
                "code": 404,
                "message": "Image not found"
            }
        ]
    )
    def get(self, id):
        image = Image.get_by_id(id)
        # return ImageSchema().jsonify(image)


class ImageListResource(Resource):
    """Image List API"""
    @swagger.operation(
        notes='Returns all images',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found images"
            },
            {
                "code": 404,
                "message": "Images not found"
            }
        ]
    )
    def get(self):
        images = Image.query.all()
        # return ImageSchema(many=True).jsonify(images)


class AudioResource(Resource):
    """Audio Class API"""
    @swagger.operation(
        notes='Returns a sound by id',
        nickname='get',
        parameters=[
            {
              "name": "id",
              "description": "The ID of the sound",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found sound"
            },
            {
                "code": 404,
                "message": "Sound not found"
            }
        ]
    )
    def get(self, id):
        sound = Audio.get_by_id(id)
        # return AudioSchema().jsonify(sound)


class AudioListResource(Resource):
    """Audio List API"""
    @swagger.operation(
        notes='Returns all sounds',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found sounds"
            },
            {
                "code": 404,
                "message": "Sounds not found"
            }
        ]
    )
    def get(self):
        sounds = Audio.query.all()
        # return AudioSchema(many=True).jsonify(sounds)


class LevelXpMapResource(Resource):
    """LevelXpMap Class API"""
    @swagger.operation(
        notes='Returns a specific XP mapping by level',
        nickname='get',
        parameters=[
            {
              "name": "level",
              "description": "The level number",
              "required": True,
              "allowMultiple": False,
              "dataType": "integer",
              "paramType": "path"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Found level XP definition"
            },
            {
                "code": 404,
                "message": "No XP defined for this level"
            }
        ]
    )
    def get(self, level):
        req = LevelExpMap.query.filter(LevelExpMap.level==level).first()
        # return LevelXpMapSchema().jsonify(req)


class LevelXpMapListResource(Resource):
    """LevelXpMap List API"""
    @swagger.operation(
        notes='Returns all XP requirements for levels',
        nickname='get',
        responseMessages=[
            {
                "code": 200,
                "message": "Found XP requirements"
            },
            {
                "code": 404,
                "message": "No XP requirements defined"
            }
        ]
    )
    def get(self):
        reqs = LevelExpMap.query.all()
        # return LevelXpMapSchema(many=True).jsonify(reqs)

api.add_resource(PlayerResource, '/players/<int:id>', endpoint='player')
api.add_resource(PlayerListResource, '/players', endpoint='players')

api.add_resource(PlayerLoginResource, '/login', endpoint='login')
api.add_resource(TokenRefresh, '/token/refresh', endpoint='token_refresh')
api.add_resource(PlayerLogoutAccess, '/logout/access', endpoint='logout_access')
api.add_resource(PlayerLogoutRefresh, '/logout/refresh', endpoint='logout_refresh')
api.add_resource(GetAuthedPlayer, '/auth/player', endpoint='get_authed_player')

api.add_resource(TeamResource, '/teams/<int:id>', endpoint='team')
api.add_resource(TeamListResource, '/teams', endpoint='teams')

api.add_resource(MobResource, '/mobs/<int:id>', endpoint='mob')
api.add_resource(MobListResource, '/mobs', endpoint='mobs')

api.add_resource(PlayerClassResource, '/classes/<int:id>', endpoint='class')
api.add_resource(PlayerClassListResource, '/classes', endpoint='classes')

api.add_resource(BountyResource, '/bounties/<int:id>', endpoint='bounty')
api.add_resource(BountyListResource, '/bounties', endpoint='bounties')

api.add_resource(CoinResource, '/coins/<int:id>', endpoint='coin')
api.add_resource(CoinListResource, '/coins', endpoint='coins')

api.add_resource(GiftResource, '/gifts/<int:id>', endpoint='gift')
api.add_resource(GiftListResource, '/gifts', endpoint='gifts')

api.add_resource(ImageResource, '/images/<int:id>', endpoint='image')
api.add_resource(ImageListResource, '/images', endpoint='images')

api.add_resource(AudioResource, '/sounds/<int:id>', endpoint='sound')
api.add_resource(AudioListResource, '/sounds', endpoint='sounds')

api.add_resource(LevelXpMapResource, '/level_xp/<int:level>', endpoint='level_xp_map')
api.add_resource(LevelXpMapListResource, '/level_xp', endpoint='level_xp_maps')