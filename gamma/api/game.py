from flask import jsonify, request, url_for, current_app
from flask_sqlalchemy import get_debug_queries
from flask_jwt_extended import create_access_token, create_refresh_token, \
    jwt_required, \
    jwt_refresh_token_required, \
    get_jwt_identity, \
    get_raw_jwt, \
    verify_jwt_in_request

from functools import wraps
from gamma.app import db
from gamma.api import api_bp as bp
from gamma.api.errors import bad_request, error_response
from gamma.models import ActivityLog, Mob, Player, PlayerClass, Bounty, Coin, \
    Gift, ImageResource, AudioResource, \
    GiftRecipientAssociations, EquipmentGift, \
    ConsumableGift, BountyPlayerAssociations, \
    MobPlayerAssociations, Equipment, Consumable, Team, RevokedTokenModel


@bp.after_request
def after_request(response):  # pragma: no cover
    for query in get_debug_queries():
        if query.duration >= current_app.config['DATABASE_QUERY_TIMEOUT']:
            print(
                """
                SLOW DATABASE QUERY: {}
                Parameters: {}
                Duration: {}
                Context: {}
                """
                .format(query.statement, query.parameters,
                        query.duration, query.context))
    return response


def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)

###############################
# Route Decorators for security
###############################


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        player_email = get_jwt_identity()
        player = Player.get_by_email(player_email)
        if player and player.email == 'none@none.com':
            return fn(*args, **kwargs)
        else:
            return jsonify(message='Admins only!'), 403
    return wrapper


def belongs_to_team(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        player_email = get_jwt_identity()
        player = Player.get_by_email(player_email)
        if player is None:
            return error_response(500, 'Player could not be found')
        if 'id' in kwargs and player.team_id == kwargs['id']:
            return fn(*args, **kwargs)
        else:
            return jsonify(message='Must be a member of this team!'), 403
    return wrapper

###############################
# Mob Routes
###############################


@bp.route('/mobs', methods=['GET'])
def get_mobs():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Mob.to_collection_dict(Mob.query, page, per_page, 'api.get_mobs')
    return jsonify(data)


@bp.route('/mobs/<int:id>', methods=['GET'])
def get_mob(id):
    return jsonify(Mob.query.get_or_404(id).to_dict())


@bp.route('/mobs/create', methods=['POST'])
def create_mob():
    data = request.get_json() or {}
    print('create_mob() data: {}'.format(data))
    if 'topic' not in data or 'location' not in data \
            or 'team_id' not in data:
        return bad_request('must include topic, location, and team_id')
    try:
        mob = Mob.create(topic=data['topic'],
                         location=data['location'],
                         team_id=data['team_id'],
                         interval_length=getattr(data, 'interval_length', 7))
        print('Successfully created mob {}!'.format(mob.id))
        if data.has_key('players'):
            for idx, p_id in enumerate(data['players']):
                player = Player.get_by_id(p_id)
                player.update(queue_position=idx+1)
                print('create_mob(): Adding player {} to mob {}'
                    .format(player.username, mob.id)
                )
                mob.players.append(player)
            mob.save()

        resp = jsonify(mob.to_dict())
        resp.status_code = 201
        resp.headers['Location'] = url_for('api.get_mob', id=mob.id)

    except Exception as e:
        resp = jsonify({
            'message': 'error creating mob',
            'error': str(e)
        })
        resp.status_code = 500
    
    return resp


@bp.route('/mobs/<int:id>/disband', methods=['GET'])
def disband_mob(id):
    mob = Mob.query.get_or_404(id)
    if mob.end_mob():
        resp = jsonify(mob.to_dict())
        resp.status_code = 201
        resp.headers['Location'] = url_for('api.get_mob', id=mob.id)
    else:
        resp = jsonify({
            'message': 'error disbanding mob',
            'error': str(e)
        })
        resp.status_code = 500

    return resp

@bp.route('/mobs/<int:id>/players/add', methods=['POST'])
def add_player_to_mob(id):
    mob = Mob.query.get_or_404(id)
    data = request.get_json() or {}
    if 'player_id' not in data:
        return bad_request('player_id is required for this request')

    if mob.add_player(data['player_id']):
        resp = jsonify(mob.to_dict())
        resp.status_code = 201
        resp.headers['Location'] = url_for('api.get_mob', id=mob.id)
    else:
        resp = jsonify({
            'message': 'Error adding player to mob'
        })
        resp.status_code = 500

    return resp

@bp.route('/mobs/<int:id>', methods=['PUT'])
def update_mob(id):
    mob = Mob.query.get_or_404(id)
    data = request.get_json() or {}
    print('update_mob() data: {}'.format(data))
    if mob.update_self(data):
        resp = jsonify(mob.to_dict())
        resp.status_code = 201
        return resp
    else:
        return error_response(500, 'error updating mob information')


@bp.route('/mobs/<int:id>/players', methods=['GET'])
def get_mob_players(id):
    mob = Mob.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Mob.to_collection_dict(mob.players, page, per_page, 
                                  'api.get_mob_players', id=mob.id)
    return jsonify(data)


@bp.route('/mobs/<int:id>/rotate-queue', methods=['GET'])
def rotate_player_queue(id):
    mob = Mob.query.get_or_404(id)
    if mob.rotate_queue():
        resp = jsonify(mob.to_dict())
        resp.status_code = 201
        return resp
    else:
        return bad_request('something went wrong when trying to rotate queue')


@bp.route('/mobs/<int:id>/timer/start')
def start_mob_timer(id):
    print("Starting timer for mob {}".format(id))
    mob = Mob.query.get_or_404(id)
    if mob.start_timer():
        resp = jsonify(mob.to_dict())
        resp.status_code = 201
    else:
        return bad_request('error starting timer')
    return resp


@bp.route('/mobs/<int:id>/timer/stop')
def stop_mob_timer(id):
    print("Stopping timer for mob {}".format(id))
    mob = Mob.query.get_or_404(id)
    if mob.stop_timer():
        resp = jsonify(mob.to_dict())
        resp.status_code = 201
    else:
        return bad_request('error stopping timer')
    return resp


###############################
# Team Routes
###############################


@bp.route('/teams', methods=['GET'])
def get_teams():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Team.to_collection_dict(Team.query, page, per_page, 'api.get_teams')
    return jsonify(data)


@bp.route('/teams/<int:id>', methods=['GET'])
def get_team(id):
    return jsonify(Team.query.get_or_404(id).to_dict())


@bp.route('/teams/<int:id>/mobs', methods=['GET'])
def get_team_mobs(id):
    team = Team.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Team.to_collection_dict(team.mobs, page, per_page, 
                                   'api.get_team_mobs', id=id)
    return jsonify(data)


@bp.route('/teams/create', methods=['POST'])
def create_team():
    data = request.get_json() or {}
    if 'name' not in data:
        return bad_request('must include team name')
    if Team.query.filter_by(name=data['name']).first():
        return bad_request('please use a different name')
    team = Team(name=data['name'])
    team.save()
    resp = jsonify(team.to_dict())
    resp.status_code = 201
    resp.headers['Location'] = url_for('api.get_team', id=team.id)
    return resp


@bp.route('/teams/<int:id>/players', methods=['GET'])
def get_team_players(id):
    team = Team.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Team.to_collection_dict(team.players, page, per_page,
                                   'api.get_team_players', id=id)
    return jsonify(data)


@bp.route('/teams/<int:id>/update', methods=['PUT'])
def update_team(id):
    team = Team.query.get_or_404(id)
    data = request.get_json() or {}
    if 'name' in data and data['name'] == team.name or \
            Team.query.filter_by(name=data['name']).first():
        return bad_request('please use a different team name')
    team.update(name=data['name'])
    resp = jsonify(team.to_dict())
    resp.status_code = 201
    resp.headers['Location'] = url_for('api.get_team', id=team.id)
    return resp


@bp.route('/teams/<int:id>/delete', methods=['DELETE'])
def delete_team(id):
    team = Team.query.get_or_404(id)
    for p in team.players:
        p.delete()
    resp = jsonify(team.to_dict())
    team.delete()
    resp.status_code = 201
    return resp


###############################
# Player Routes
###############################

@bp.route('/players', methods=['GET'])
def get_players():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Player.to_collection_dict(Player.query, page, 
                                     per_page, 'api.get_players')
    return jsonify(data)


@bp.route('/players/<int:id>', methods=['GET'])
def get_player(id):
    return jsonify(Player.query.get_or_404(id).to_dict())


@bp.route('/players/<int:id>', methods=['PUT'])
def update_player(id):
    player = Player.query.get_or_404(id)
    data = request.get_json() or {}
    # TODO add checks for duplicate username, email, etc
    if player.update_self(data):
        resp = jsonify(player.to_dict())
        resp.status_code = 201
        return resp
    else:
        return error_response(500, 'error updating player information')


@bp.route('/players/create', methods=['POST'])
def create_player():
    data = request.get_json() or {}

    if 'username' not in data or \
       'email' not in data or \
       'password' not in data or \
       'team_id' not in data:
        return bad_request(
            'must include player username, email, password, and team')

    if data['username'] in [p.username
                            for p
                            in Team.query.get_or_404(data['team_id']).players]:
        return bad_request('player with username already exists in team')

    player = Player(username=data['username'],
                    password=data['password'],
                    team_id=data['team_id'],
                    email=data['email'])
    player.save()
    resp = jsonify(player.to_dict())
    resp.access_token = player.access_token
    resp.refresh_token = player.refresh_token
    resp.status_code = 201
    return resp
    

@bp.route('/players/<int:id>/xp/add', methods=['POST'])
def add_xp_to_player(id):
    data = request.get_json() or {}

    if 'xp' not in data:
        return bad_request("Route requires an xp field to be submitted")

    player = Player.query.get_or_404(id)
    player.update_xp(data['xp'])

    resp = jsonify(player.to_dict())
    resp.status_code = 201
    return resp
    
###############################
# Auth Routes
###############################

@bp.route('/auth/login', methods=['POST'])
def login_player():
    data = request.get_json() or {}

    if 'email' not in data or 'password' not in data:
        return bad_request('email and password are required to login')

    player = Player.get_by_email(data['email'])
    if not player:
        # should we be showing which emails are/are not in the db?
        return bad_request('player does not exist')

    if player.check_password(data['password']):
        # player.create_tokens()
        new_access_token = create_access_token(identity=player.email, fresh=True)
        new_refresh_token = create_refresh_token(identity=player.email)
        player.update(access_token=new_access_token)
        player.update(refresh_token=new_refresh_token)
        resp = jsonify(player.to_dict(with_tokens=True))
        # resp = jsonify({
        #     'access_token': player.access_token,
        #     'refresh_token': player.refresh_token
        # })
        resp.status_code = 200
        return resp
    
    return error_response(401, 'wrong email address or password')


@bp.route('/auth/logout', methods=['DELETE'])
def logout_access():
    jti = get_raw_jwt()['jti']
    try:
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.add()
        resp = jsonify({
            'message': 'Access token has been revoked'
        })
        resp.status_code = 200
        return resp
    except:
        return error_response(500,
                              'Something went wrong revoking access token')


@bp.route('/auth/logout/refresh', methods=['DELETE'])
def logout_refresh():
    jti = get_raw_jwt()['jti']
    try:
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.add()
        resp = jsonify({
            'message': 'Refresh token has been revoked'
        })
        resp.status_code = 200
        return resp
    except:
        return error_response(500,
                              'Something went wrong revoking refresh token')


@bp.route('/auth/refresh', methods=['POST'])
def refresh_token():
    """From Flask-JWT-Extended's docs: 
    This will generate a new access token from
    the refresh token, but will mark that access token as non-fresh,
    as we do not actually verify a password in this endpoint.
    """
    player_email = get_jwt_identity()
    print("refresh_token: Found player email {}".format(player_email))
    player = Player.get_by_email(player_email)
    if player:
        new_access_token = create_access_token(identity=player_email, fresh=False)
        player.update(access_token=new_access_token)
        resp = jsonify({
            'access_token': new_access_token
        })
        resp.status_code = 200
        return resp
    else:
        return error_response(500, 'could not refresh access token for given email address')

@bp.route('/auth/test-jwt')
def test_auth_jwt():
    player_email = get_jwt_identity()
    resp = jsonify({
        'player_email': player_email,
        'message': 'protected niew works!'
    })
    resp.status_code = 200
    return resp