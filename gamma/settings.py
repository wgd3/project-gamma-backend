import os
from datetime import timedelta
from dotenv import load_dotenv

APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
load_dotenv(os.path.join(PROJECT_ROOT, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') \
        or 'somethingdarkside'
    SERVER_NAME = os.environ.get('SERVER_NAME') or None
    # JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY') \
    #     or 'somethingsomethingdarkside'
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT') \
    #     or 'somethingsomethingsomethingdarkside'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # slow query threshold (in seconds)
    DATABASE_QUERY_TIMEOUT = 0.5

    # Flask-JWT-Extended Options
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    # JWT_TOKEN_LOCATION = 'headers'
    # JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=60)
    # JWT_REFRESH_TOKEN_EXPIRES = timedelta(minutes=60)

    # Email Options
    # for dev purposes you can use a local SMTP server:
    # python -m smtpd -n -c DebuggingServer localhost:8025
    # MAIL_SERVER = os.environ.get('MAIL_SERVER') \
    #     or 'localhost'
    # MAIL_PORT = os.environ.get('MAIL_PORT') \
    #     or 8025
    # MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') \
    #     or 0
    # MAIL_USE_SSL = os.environ.get('MAIL_USE_SSL') \
    #     or 1
    # MAIL_USERNAME = os.environ.get('MAIL_USERNAME') \
    #     or 'your-email-username'
    # MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') \
    #     or 'your-email-password'


class DevConfig(Config):
    ENV = os.environ.get('ENV') or 'dev'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(PROJECT_ROOT, 'dev.db')
    # JWT_ACCESS_TOKEN_EXPIRES = False
    # JWT_REFRESH_TOKEN_EXPIRES = False
    SQLALCHEMY_ECHO = False


class ProdConfig(Config):
    ENV = os.environ.get('ENV') or 'prod'
    DEBUG = False
    WTF_CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(PROJECT_ROOT, 'app.db')


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_ECHO = False