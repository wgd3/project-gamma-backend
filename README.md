# Project GAMMA Backend #

## Set Up ##

1) Create a virtual environment to install dependencies:
```
$ virtualenv venv
```

2) Activate the venv and install requirements:
```
$ source venv/bin/activate
$ pip install -r requirements.txt
```

3) `settings.py` contains default (SQLite) databases, but certain settings can be overwritten by creating a `.env` file in the project root:
```
$ vim .env

DATABASE_URL=postgresql://user:pass@ip:port/dbname
SECRET_KEY=mysupersecureandcomplexsecretkey
```

4) Export Flask shell variables for the CLI _(use SET on Windows)_:
```
$ export FLASK_APP=autoapp.py
$ export FLASK_DEBUG=1
```

5) Initialize the database:
```
$ flask db upgrade
```

6) Back in the first terminal, run GAMMA!
```
$ flask run
```

### Extra Setup Notes ###

#### `activate` alterations ####

In an effort to save a few keystrokes, you can update the virtualenv's `activate` script to auto-set the FLASK_* variables:
```
VIRTUAL_ENV="/Users/wallace/git/project-gamma/angular_venv"
export VIRTUAL_ENV

FLASK_APP=autoapp.py
export FLASK_APP

FLASK_DEBUG=1
export FLASK_DEBUG
```
These variables need to be unset when the virtualenv is deactivated, so add these lines into the `deactivate()` function:
```
deactivate () {
    ...
    unset VIRTUAL_ENV
    unset FLASK_APP
    unset FLASK_DEBUG
    ...
}
```

## Testing ##

All tests for the backend can be found in the `tests` folder in your project root. A command has been added to the Flask CLI for easy testing:
```
$ flask test
```
Currently, given how many functions each model has, there is a separate `test_*.py` file for each model that the API talks to. The database model tests require less code for unit tests, so for now they all live in the singular `test_models.py` file. 

If you run a coverage report using the Flask CLI, you will notice that the coverage report does not reprsent the various blueprint route definitions - this is due to the fact that the app is loaded (and those routes registered/executed) before the test runs the associated route's functional code. See [this](http://coverage.readthedocs.io/en/coverage-4.2/faq.html) for more information.

The following commands will run all unit tests with complete coverage, as well as generate a terminal and HTML report:
```
$ coverage run -m unittest discover -s tests/ 
$ coverage report -m
$ coverage html
```
