FROM python:2.7-slim-jessie
MAINTAINER Wallace Daniel "wallace.daniel3@me.com"

RUN adduser -q flask
WORKDIR /home/flask

COPY requirements.txt requirements.txt
RUN apt-get update -y && \
    apt-get install -y python-pip python-virtualenv libpq-dev npm node && \
    npm install -g npm@5 && \
    virtualenv venv && \
    venv/bin/pip install -U setuptools && \
    venv/bin/pip install -r requirements.txt && \
    venv/bin/pip install gunicorn
    

COPY . .

WORKDIR /home/flask/gamma/frontend
RUN npm install && \
    ng build --deploy-url=game/static/

WORKDIR /home/flask
RUN chown -R flask:flask ./
RUN chmod +x docker-boot.sh
ENV FLASK_APP autoapp.py
ENV FLASK_DEBUG 1
USER flask

EXPOSE 5000
ENTRYPOINT ["./docker-boot.sh"]

