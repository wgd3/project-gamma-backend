#!/usr/bin/env python
from datetime import datetime, timedelta
import unittest
from gamma.app import create_app, db
from gamma.models import Mob, Player, Team
from gamma.settings import TestConfig


class MobModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.team = Team.create(name='Test Team')

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_default_interval_length(self):
        m = Mob('Test Topic', 'Test Location', self.team.id)
        m.save()
        self.assertEquals(m.interval_length, 7)