import unittest
import os
import json
from gamma.app import create_app, db
from gamma.settings import TestConfig
from gamma.models import Team, Player, PlayerClass, LevelExpMap


class TeamApiTestCase(unittest.TestCase):

    url_base = '/api/v1'

    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.test_pc = PlayerClass.create(name='test class')
        self.test_team = Team.create(name='test team')
        self.test_player = Player.create(
            username="testuser00",
            email='testuser00@none.com',
            password='badpassword',
            team_id=self.test_team.id
        )
        # use test_player access tokens for API requests
        self.headers = {
            'Authorization': 'Bearer {}'.format(self.test_player.access_token)
        }
        self.test_lvl_xp_map = LevelExpMap.create(level=50, req_xp=1800)

    def test_get_teams(self):
        test_url = self.url_base + '/teams'
        res = self.client().get(test_url)
        res_json = json.loads(res.data.decode('utf-8'))

        self.assertEqual(res.status_code, 200)
        self.assertIn('items', res_json)
        self.assertEqual(len(res_json['items']), 1)

    def test_get_team_by_id(self):
        test_url = self.url_base + '/teams/'
        with self.app.app_context():
            test_url = test_url + str(self.test_team.id)
            res = self.client().get(test_url, headers=self.headers)
            self.assertEqual(res.status_code, 200)

    def test_create_team(self):
        test_url = self.url_base + '/teams/create'
        test_team = {
            'name': 'Test Create Team',
        }
        res = self.client().post(test_url, 
                                 data=json.dumps(test_team),
                                 content_type='application/json')
        res_json = json.loads(res.data.decode('utf-8'))
        self.assertEqual(res.status_code, 201)

    def test_malformed_new_team_returns_400(self):
        test_url = self.url_base + '/teams/create'
        test_team = {
            'foo': 'bar',
        }
        res = self.client().post(test_url, 
                                 data=json.dumps(test_team),
                                 content_type='application/json')
        res_json = json.loads(res.data.decode('utf-8'))
        self.assertEqual(res.status_code, 400)
        self.assertIn('name', res_json['message'])

    def test_get_team_players(self):
        test_url = self.url_base + '/teams/'
        with self.app.app_context():
            # t = Team(name='Test Team Name')
            # t.save()
            # pc = PlayerClass('test class')
            # pc.save()
            for i in xrange(3):
                p = Player(username='Player{}'.format(i),
                           password='password',
                           email='none{}@none.com'.format(i),
                           team_id=self.test_team.id)
                p.save()
            
            test_url = test_url + '{}/players'.format(self.test_team.id)
            res = self.client().get(test_url, headers=self.headers)
            res_json = json.loads(res.data.decode('utf-8'))

            self.assertEqual(res.status_code, 200)
            # for loop +1 for player in setUp()
            self.assertEqual(len(res_json['items']), 4)

    def test_duplicate_team_name(self):
        test_url = self.url_base + '/teams/create'
        test_team = {
            'name': 'Test Team Name',
        }
        with self.app.app_context():
            t = Team(name='Test Team Name')
            t.save()
            res = self.client().post(test_url, 
                                     data=json.dumps(test_team),
                                     content_type='application/json')
            res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 400)

    def test_update_team_name(self):
        test_url = self.url_base + '/teams'
        updated_team = {
            'name': 'Updated Team Name'
        }
        with self.app.app_context():
            t = Team(name='Update Test')
            t.save()
            test_url = test_url + '/{}/update'.format(t.id)
            res = self.client().put(test_url,
                                    data=json.dumps(updated_team),
                                    content_type='application/json')
            # res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 201)

    def test_update_team_name_incorrectly(self):
        test_url = self.url_base + '/teams'
        with self.app.app_context():
            t = Team(name='Updated Team Name')
            t.save()
            test_url = test_url + '/{}/update'.format(t.id)
            updated_team = {
                'name': t.name
            }
            res = self.client().put(test_url,
                                    data=json.dumps(updated_team),
                                    content_type='application/json')
            # res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 400)

    def test_delete_team(self):
        test_url = self.url_base + '/teams'
        with self.app.app_context():
            t = Team(name='Delete Test')
            t.save()
            pc = PlayerClass('test class')
            pc.save()
            for i in xrange(3):
                p = Player(username='Player{}'.format(i),
                           password='password',
                           email='none{}@none.com'.format(i),
                           team_id=self.test_team.id)
                p.save()
            test_url = test_url + '/{}/delete'.format(t.id)
            res = self.client().delete(test_url)
            res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 201)

    def test_delete_null_team(self):
        test_url = self.url_base + '/teams/99/delete'
        res = self.client().delete(test_url)
        self.assertEqual(res.status_code, 404)

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()