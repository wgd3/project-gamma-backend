import unittest
import os
import json
from gamma.app import create_app, db
from gamma.settings import TestConfig
from gamma.models import Player, RevokedTokenModel, Team


class JwtApiTestCase(unittest.TestCase):

    url_base = '/api/v1'
    player = None
    team = None

    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.team = Team.create(name='test team')
        self.player = Player.create(username='tester',
                                    password='badpass',
                                    email='none@none.com',
                                    team_id=self.team.id)
                                    
    def execute_player_login(self, test_data=None):
        test_url = self.url_base + '/auth/login'
        if test_data:
            data = test_data
        else:
            data = {
                'email': 'none@none.com',
                'password': 'badpass'
            }
        res = self.client().post(test_url,
                                 data=json.dumps(data),
                                 content_type='application/json')
        res_json = json.loads(res.data.decode('utf-8'))
        return res, res_json

    def test_player_login(self):
        res, res_json = self.execute_player_login()
        self.assertEqual(res.status_code, 200)

    def test_fake_player_login(self):
        data = {
            'email': '99@none.com',
            'password': 'badpass'
        }
        res, res_json = self.execute_player_login(data)
        self.assertEqual(res.status_code, 400)

    def test_login_without_password(self):
        data = {
            'email': '99@none.com',
        }
        res, res_json = self.execute_player_login(data)
        self.assertEqual(res.status_code, 400)

    def test_log_out_access_token(self):
        res, res_json = self.execute_player_login()
        self.assertEqual(res.status_code, 200)
        self.assertIn('access_token', res_json)

        test_access_token = res_json['access_token']
        test_url = self.url_base + '/auth/logout'
        res = self.client().delete(
            test_url,
            {},
            headers=dict(
              Authorization='Bearer {0}'.format(test_access_token)
            )
        )
        # res_json = json.loads(res.data.decode('utf-8'))
        self.assertEqual(res.status_code, 200)

        def test_log_out_access_token(self):
            test_url = self.url_base + '/auth/login'
            data = {
                'email': 'none@none.com',
                'password': 'badpass'
            }
            res = self.client().post(test_url,
                                    data=json.dumps(data),
                                    content_type='application/json')
            res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 200)
            self.assertIn('refresh_token', res_json)

            test_refresh_token = res_json['refresh_token']
            test_url = self.url_base + '/auth/logout'
            res = self.client().delete(
                test_url,
                {},
                headers=dict(
                Authorization='Bearer {0}'.format(test_refresh_token)
                )
            )
            res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 200)

        def test_create_refresh_token(self):
            test_url = self.url_base + '/auth/login'
            data = {
                'email': 'none@none.com',
                'password': 'badpass'
            }
            res = self.client().post(test_url,
                                    data=json.dumps(data),
                                    content_type='application/json')
            res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 200)
            self.assertIn('refresh_token', res_json)

            orig_access_token = res_json['access_token']
            refresh_token = res_json['refresh_token']

            test_url = self.url_base + '/token/refresh'
            res = self.client().post(
                test_url,
                {},
                headers=dict(
                Authorization='Bearer {0}'.format(test_refresh_token)
                )
            )
            res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 200)
            self.assertNotEqual(orig_access_token, res_json['access_token'])

    # TODO https://realpython.com/token-based-authentication-with-flask/#code-smell
    # finish writing bad/malformed token tests

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

            
if __name__ == '__main__':
    try:
        unittest.main()
    except:
        pass