import unittest
import os
import json
from gamma.app import create_app, db
from gamma.settings import TestConfig
from gamma.models import Mob, Team, Player


class MobApiTestCase(unittest.TestCase):

    url_base = '/api/v1'

    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.team = Team.create(name='Test Team')

    def test_get_mobs(self):
        test_url = self.url_base + '/mobs'
        res = self.client().get(test_url)
        res_json = json.loads(res.data.decode('utf-8'))
        # print res_json
        # print("res data: " + res.data)
        self.assertEqual(res.status_code, 200)
        self.assertIn('items', res_json)
        self.assertEqual(len(res_json['items']), 0)

    def test_get_mob_by_id(self):
        test_url = self.url_base + '/mobs/'
        with self.app.app_context():
            m = Mob('Test Topic', 'Test Location', team_id=self.team.id)
            m.save()
            test_url = test_url + str(m.id)
            res = self.client().get(test_url)
            self.assertEqual(res.status_code, 200)

    def test_create_mob(self):
        test_url = self.url_base + '/mobs/create'
        test_mob = {
            'topic': 'Test Create Mob',
            'location': 'Test Create Location',
            'team_id': self.team.id
        }
        res = self.client().post(test_url, 
                                 data=json.dumps(test_mob),
                                 content_type='application/json')
        res_json = json.loads(res.data.decode('utf-8'))
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res_json['active'], True)

    def test_create_mob_with_players(self):
        test_url = self.url_base + '/mobs/create'
        test_mob = {
            'topic': 'Test Create Mob',
            'location': 'Test Create Location',
            'team_id': self.team.id,
            'players': []
        }
        res = self.client().post(test_url,
                                 data=json.dumps(test_mob),
                                 content_type='application/json')
        res_json = json.loads(res.data.decode('utf-8'))
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res_json['active'], True)

        for i in xrange(3):
            player = Player.create(
                username='player{}'.format(i),
                email='player{}@none.com'.format(i),
                password='password',
                team_id=self.team.id)
            res = self.client().post('/api/v1/mobs/{}/players/add'.format(res_json['id']),
                                     data=json.dumps({'player_id': player.id}),
                                     content_type='application/json')
            res_json = json.loads(res.data.decode('utf-8'))
            self.assertEqual(res.status_code, 201)


    def test_malformed_new_mob_returns_400(self):
        test_url = self.url_base + '/mobs/create'
        test_mob = {
            'topic': 'Test Create Mob',
        }
        res = self.client().post(test_url, 
                                 data=json.dumps(test_mob),
                                 content_type='application/json')
        res_json = json.loads(res.data.decode('utf-8'))
        self.assertEqual(res.status_code, 400)
        self.assertIn('location', res_json['message'])

if __name__ == '__main__':
    try:
        unittest.main()
    except:
        pass