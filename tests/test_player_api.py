import unittest
import os
import json
from gamma.app import create_app, db
from gamma.settings import TestConfig
from gamma.models import Team, Player, PlayerClass, Mob, LevelExpMap


class PlayerApiTestCase(unittest.TestCase):

    url_base = '/api/v1/players'
    test_pc = None
    test_team = None
    test_lvl_xp_map = None

    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.test_pc = PlayerClass.create(name='test class')
        self.test_team = Team.create(name='test team')
        self.test_lvl_xp_map = LevelExpMap.create(level=50, req_xp=1800)

    def test_get_players(self):
        res = self.client().get(self.url_base)
        res_json = json.loads(res.data.decode('utf-8'))
        # print res_json
        # print("res data: " + res.data)
        self.assertEqual(res.status_code, 200)
        self.assertIn('items', res_json)
        self.assertEqual(len(res_json['items']), 0)

    def test_get_player_by_id(self):
        with self.app.app_context():
            p = Player(username='Player1',
                       password='Password1',
                       email='none@none.com',
                       team_id=self.test_team.id)
            p.save()
            test_url = self.url_base + '/{}'.format(p.id)
            res = self.client().get(test_url)
            self.assertEqual(res.status_code, 200)

    def test_get_null_player(self):
        test_url = self.url_base + '/99'
        res = self.client().get(test_url)
        self.assertEqual(res.status_code, 404)

    def test_create_player(self):
        test_url = self.url_base + '/create'
        test_player = {
            'username': 'Player1',
            'email': 'none@none.com',
            'password': 'Password1',
            'team_id': self.test_team.id
        }
        res = self.client().post(test_url, 
                                 data=json.dumps(test_player),
                                 content_type='application/json')
        res_json = json.loads(res.data.decode('utf-8'))
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res_json['xp'], 0)
        self.assertEqual(res_json['all_time_xp'], 0)
        self.assertEqual(res_json['team_admin'], False)

    def test_create_player_without_username(self):
        test_url = self.url_base + '/create'
        test_player = {
            'class_id': self.test_pc.id,
            'team_id': self.test_team.id
        }
        res = self.client().post(test_url, 
                                 data=json.dumps(test_player),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 400)

    def test_create_duplicate_player_in_team(self):
        test_url = self.url_base
        test_player = {
            'data': {
                'username': 'Player1',
                'email': 'none@none.com',
                'password': 'Password1',
                'team_id': self.test_team.id
            }
        }
        with self.app.app_context():
            p = Player(username='Player1',
                       password='Password1',
                       email='none@none.com',
                       team_id=self.test_team.id)
            p.save()
        res = self.client().post(test_url, 
                                 data=json.dumps(test_player),
                                 content_type='application/json')
        print('test res: {}'.format(res))
        res_json = json.loads(res.data)
        self.assertEqual(res.status_code, 400)
        self.assertIn('already', res_json['message'])

    def test_add_xp_to_player(self):
        xp_data = {
            'xp': 100
        }
        with self.app.app_context():
            p = Player(username='Player1',
                       password='Password1',
                       email='none@none.com',
                       team_id=self.test_team.id)
            p.save()

            test_url = self.url_base + '/{}/xp/add'.format(p.id)
            res = self.client().post(test_url,
                                     data=json.dumps(xp_data),
                                     content_type='application/json')
            res_json = json.loads(res.data.decode('utf-8'))

            self.assertEqual(res.status_code, 201)
            self.assertEqual(xp_data['xp'], res_json['xp'])

    def test_add_xp_to_null_player(self):
        xp_data = {
            'xp': 100
        }
        test_url = self.url_base + '/99/xp/add'
        res = self.client().post(test_url,
                                 data=json.dumps(xp_data),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def test_bad_xp_addition(self):
        xp_data = {
            
        }
        with self.app.app_context():
            p = Player(username='Player1',
                       password='Password1',
                       email='none@none.com',
                       team_id=self.test_team.id)
            p.save()

            test_url = self.url_base + '/{}/xp/add'.format(p.id)
            res = self.client().post(test_url,
                                     data=json.dumps(xp_data),
                                     content_type='application/json')

            self.assertEqual(res.status_code, 400)

    def test_level_up_player_from_xp(self):
        """The initial migration sets all levels to require
        1800XP by default"""
        xp_data = {
            'xp': 2000
        }
        with self.app.app_context():
            p = Player(username='Player1',
                       password='Password1',
                       email='none@none.com',
                       team_id=self.test_team.id)
            p.save()

            test_url = self.url_base + '/{}/xp/add'.format(p.id)
            res = self.client().post(test_url,
                                     data=json.dumps(xp_data),
                                     content_type='application/json')
            res_json = json.loads(res.data.decode('utf-8'))

            self.assertEqual(res.status_code, 201)
            self.assertEqual(200, res_json['xp'])
            self.assertEqual(2, res_json['level'])
            self.assertEqual(2000, res_json['all_time_xp'])

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()